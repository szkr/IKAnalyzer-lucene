# 第一部分 简介

`IKAnalyzer-lucene`是基于`ik-analyzer`分词器，在其基础上实现对不同版本的`lucene`兼容，`ik-analyzer`项目主页地址为：[https://code.google.com/archive/p/ik-analyzer/](https://code.google.com/archive/p/ik-analyzer/)。

# 第二部分 开始使用
使用`IKAnalyzer-lucene`可以直接下载源代码编译或者下载已经编译的`jar`文件，如果您是使用`maven`来构建项目，也可以直接在`pom.xml`中添加`JHttp`的坐标：

[![Maven central](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/IKAnalyzer-lucene/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/IKAnalyzer-lucene)

```xml
<!-- http://mvnrepository.com/artifact/com.jianggujin/IKAnalyzer-lucene -->
<dependency>
    <groupId>com.jianggujin</groupId>
    <artifactId>IKAnalyzer-lucene</artifactId>
    <version>lucene版本</version>
</dependency>
```

最新的版本可以从[Maven仓库](http://mvnrepository.com/artifact/com.jianggujin/IKAnalyzer-lucene)或者[码云](https://gitee.com/jianggujin)获取。

`IKAnalyzer-lucene`兼容`lucene4.0.0`与之后的版本，版本号与`lucene`版本一致，分支代码为各版本对应源码，如果找不到对应版本的兼容包，则可以引用该版本以下与之相邻的版本。

